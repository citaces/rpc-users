package storages

import (
	"gitlab.com/citaces/rpc-users/internal/db/adapter"
	"gitlab.com/citaces/rpc-users/internal/infrastructure/cache"
	vstorage "gitlab.com/citaces/rpc-users/internal/modules/auth/storage"
	ustorage "gitlab.com/citaces/rpc-users/internal/modules/user/storage"
)

type Storages struct {
	User   ustorage.Userer
	Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User:   ustorage.NewUserStorage(sqlAdapter, cache),
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
