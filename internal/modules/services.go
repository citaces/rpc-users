package modules

import (
	"gitlab.com/citaces/rpc-users/internal/infrastructure/component"
	aservice "gitlab.com/citaces/rpc-users/internal/modules/auth/service"
	uservice "gitlab.com/citaces/rpc-users/internal/modules/user/service"
	"gitlab.com/citaces/rpc-users/internal/storages"
)

type Services struct {
	User          uservice.Userer
	Auth          aservice.Auther
	UserClientRPC uservice.Userer
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	userService := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		User: userService,
		Auth: aservice.NewAuth(userService, storages.Verify, components),
	}
}
