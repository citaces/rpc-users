package service

import (
	"context"
	"gitlab.com/citaces/rpc-users/internal/infrastructure/errors"
	uservice "gitlab.com/citaces/rpc-users/internal/modules/user/service"
	"log"
	"net/rpc"
)

type AuthServiceJSONRPC struct {
	client *rpc.Client
}

func NewAuthServiceJSONRPC(client *rpc.Client) *AuthServiceJSONRPC {
	a := &AuthServiceJSONRPC{client: client}
	return a
}

func (a *AuthServiceJSONRPC) Register(ctx context.Context, in RegisterIn, field int) RegisterOut {
	var out RegisterOut
	err := a.client.Call("AuthServiceJSONRPC.Register", []interface{}{in, field}, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceUserAlreadyExists
	}
	return out
}

func (a *AuthServiceJSONRPC) AuthorizeEmail(ctx context.Context, in AuthorizeEmailIn) AuthorizeOut {
	var out AuthorizeOut
	err := a.client.Call("AuthServiceJSONRPC.AuthorizeEmail", in, &out)
	if err != nil {
		out.ErrorCode = errors.NotifyEmailSendErr
	}
	return out
}

func (a *AuthServiceJSONRPC) AuthorizeRefresh(ctx context.Context, in AuthorizeRefreshIn) AuthorizeOut {
	var out AuthorizeOut
	err := a.client.Call("AuthServiceJSONRPC.AuthorizeRefresh", in, &out)
	if err != nil {
		out.ErrorCode = errors.AuthServiceRefreshTokenGenerationErr
	}
	return out
}

func (a *AuthServiceJSONRPC) AuthorizePhone(ctx context.Context, in AuthorizePhoneIn) AuthorizeOut {
	var out AuthorizeOut
	err := a.client.Call("AuthServiceJSONRPC.AuthorizePhone", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceWrongPhoneCodeErr
	}
	return out
}

func (a *AuthServiceJSONRPC) SendPhoneCode(ctx context.Context, in SendPhoneCodeIn) SendPhoneCodeOut {
	var out SendPhoneCodeOut
	err := a.client.Call("AuthServiceJSONRPC.SendPhoneCode", in, &out)
	if err != nil {
		out.Code = errors.UserServiceWrongPhoneCodeErr
	}
	return out
}

func (a *AuthServiceJSONRPC) VerifyEmail(ctx context.Context, in VerifyEmailIn) VerifyEmailOut {
	var out VerifyEmailOut
	err := a.client.Call("AuthServiceJSONRPC.VerifyEmail", in, &out)
	if err != nil {
		out.ErrorCode = errors.NotifyEmailSendErr
	}
	return out
}

func (a *AuthServiceJSONRPC) SetUserer(user uservice.Userer) {
	err := a.client.Call("AuthServiceJSONRPC.SetUserer", user, nil)
	if err != nil {
		log.Fatalf("AuthServiceJSONRPC SetUserer error %v", err)
	}
}

/*
func (t *UserServiceJSONRPC) Create(ctx context.Context, in UserCreateIn) UserCreateOut {
	var out UserCreateOut
	err := t.client.Call("UserServiceJSONRPC.CreateUser", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceCreateUserErr
	}

	return out
}

func (t *UserServiceJSONRPC) Update(ctx context.Context, in UserUpdateIn) UserUpdateOut {
	var out UserUpdateOut
	err := t.client.Call("UserServiceJSONRPC.UpdateUser", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceUpdateErr
	}

	return out
}

func (t *UserServiceJSONRPC) VerifyEmail(ctx context.Context, in UserVerifyEmailIn) UserUpdateOut {
	var out UserUpdateOut
	err := t.client.Call("UserServiceJSONRPC.VerifyEmail", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceUpdateErr
	}

	return out
}

func (t *UserServiceJSONRPC) ChangePassword(ctx context.Context, in ChangePasswordIn) ChangePasswordOut {
	var out ChangePasswordOut
	err := t.client.Call("UserServiceJSONRPC.ChangePassword", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceChangePasswordErr
	}

	return out
}

func (t *UserServiceJSONRPC) GetByEmail(ctx context.Context, in GetByEmailIn) UserOut {
	var out UserOut
	err := t.client.Call("UserServiceJSONRPC.GetUserByEmail", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceRetrieveUserErr
	}

	return out
}

func (t *UserServiceJSONRPC) GetByPhone(ctx context.Context, in GetByPhoneIn) UserOut {
	var out UserOut
	err := t.client.Call("UserServiceJSONRPC.GetUserByPhone", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceRetrieveUserErr
	}

	return out
}

func (t *UserServiceJSONRPC) GetByID(ctx context.Context, in GetByIDIn) UserOut {
	fmt.Println(`IM IN RPC`)
	fmt.Println(`IM IN RPC`)
	fmt.Println(`IM IN RPC`)
	var out UserOut
	err := t.client.Call("UserServiceJSONRPC.GetUserByID", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceRetrieveUserErr
	}
	return out
}

func (t *UserServiceJSONRPC) GetByIDs(ctx context.Context, in GetByIDsIn) UsersOut {
	var out UsersOut
	err := t.client.Call("UserServiceJSONRPC.GetUsersByID", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceRetrieveUsersErr
	}
	return out
}
*/
