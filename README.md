# Go rpc

This is a simple example of how to use rpc in go.
```shell
service gateway:

https://gitlab.com/citaces/rpc-gateway

microservice auth:

https://gitlab.com/citaces/rpc-auth

docker-compose up --build
```

